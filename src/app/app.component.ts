import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from 'src/shared/services/http.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  employees$: Observable<any>;
  employeeId: any

  constructor(private http: HttpService) {}

  ngOnInit() {
    this.employees$ = this.http.getEmployees();
  }
  selectedId(id: any) {
    this.employeeId = id;
  }
}
