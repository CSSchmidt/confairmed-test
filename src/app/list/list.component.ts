import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from 'src/shared/services/http.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  @Input() elements: any;
  @Input() headerText: string;
  @Output() elementOutput = new EventEmitter<string>();
  
  constructor(private http: HttpService) {}

  ngOnInit(): void {}

  selectElement(value: string) {
    this.elementOutput.emit(value);
  }
}
