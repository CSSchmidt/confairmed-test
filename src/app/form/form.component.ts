import { Component, Input, OnInit, OnChanges } from '@angular/core';
import { Observable, pipe } from 'rxjs';
import { HttpService } from 'src/shared/services/http.service';
import { Employee } from '../../shared/entities/employee';

const salutation = [
  { displayName: 'Frau', value: 'female' },
  { displayName: 'Divers', value: 'diverse' },
  { displayName: 'Herr', value: 'male' }
]

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit, OnChanges {

  @Input() employeeId = undefined;
  employee$: Observable<any>;
  employee = new Employee();
  salutation = salutation;
  regexPhone = new RegExp(/^\+[0-9]{7,}$/);
  regexEmail = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);


  constructor(private http: HttpService) { }

  ngOnInit(): void {}

  ngOnChanges(): void {
    if (this.employeeId !== undefined) {
      this.getEmployee(this.employeeId);
    }
  }

  getEmployee(id: any) {
    this.http.getEmployee(id).subscribe(
      data => this.employee = data,
      error => console.log(error),
    );
  }

  clear() {
    this.employee = new Employee();
  }

  save() {
    if( this.regexPhone.test(this.employee.phone) && this.regexEmail.test(this.employee.email)) {
      let tmpEmployee = {
        salutation: this.employee.salutation,
        name: this.employee.name,
        phone: this.employee.phone,
        email: this.employee.email,
        medicalStoreId: this.employee.medicalStoreId,
      }
  
      this.http.addEmployee(tmpEmployee).subscribe( );
    } else {
      console.log('Ein Fehler ist aufgetreten')
    }
    
  }

  edit() {
    if( this.regexPhone.test(this.employee.phone) && this.regexEmail.test(this.employee.email)) {
      let tmpEmployee = {
        salutation: this.employee.salutation,
        name: this.employee.name,
        phone: this.employee.phone,
        email: this.employee.email,
        version: this.employee.version
      }
      this.http.editEmployee(tmpEmployee, this.employeeId).subscribe();
    } else {
      console.log('Ein Fehler ist aufgetreten')
    }
  }

  delete() {
    this.http.deleteEmployee(this.employeeId).subscribe();
  }
}
