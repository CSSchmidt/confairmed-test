export class Employee {
    salutation: string = '';
    name: string = '';
    phone: string = '';
    email: string = '';
    medicalStoreId: string = '00000000-0001-4000-ad00-000000000001';
    version: number = null;
    roleIds: string[] = [];
    createdAt: string = '';
}