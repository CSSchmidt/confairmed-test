import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Employee } from '../entities/employee';
import { catchError } from 'rxjs/operators';

const auth_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJhdXRoZW50aWNhdGlvbiIsInN1YiI6IjkxZTBmNzJjLWU2ZjEtNDBjNy05YTc4LWYxZDhhMGJmNjBjYiIsImF1ZCI6IjkyZGIyODM5LTg2NmEtNGEyZi04NzY5LTZhNzZkMjlhYzQwZCIsImp0aSI6IjIyMDU4MjFiYmJmMzJkYmRiZDJiNzg4ZTZkYWE2Y2VjOTU3ZGIwOTA0MGI0YmNkOGFlMDY1Nzg4M2UzNjJkOWE5ZmU4ZGMzYTFlMjI4MzQyIiwiaWF0IjoxNjI5NzA4OTIzLCJuYmYiOjE2Mjk3MDg5MjMsImV4cCI6MTYyOTc4MDkyMywic2NvcGVzIjpbXSwicmVmZXJyZXIiOiJodHRwOi8vbG9jYWxob3N0OjQyMDAifQ.hbJfd9aVNVHPL37msyLX8_uORF308n-w4TIgzYDw_ZFOfCxN0xKasPJQPwpnuT5EYGMhKt-f4cSOdfpKijzDBtUt6Kc63qbyndLsgFSRhBqsW67UNFqHeRI4ZXoJTSDMh_-5JwVNWwnOohjLppMD1jHh_gVIzV9Z1aUjZVMuUiuDgMxvK8rKaP8JqvL3zoXgLEr_x3ey2X5VXD5yUJ_serA0Fjrk8TyljU0zlJJffrqSJIiSuyJkjxlHJ3rfgXuhYHyIq-GhBPrZsA2LyCgueNCnVRz2ZWVPo9zj6u-_a89GRScrnXorJWE86CVdwbZura_W-xgwKXEEydQkf6v3ngO7EuouC9s-k7cVynhYiFGBJrvi-515TUxYalsd2smFhlgbl0McpcQYMw8dex7UwFYxw51IrlEYU4c7gDvYV5FoWsZoMTsiK9fT3NeXYX8PsvJIqYQTsxkSyHQmO7ytWw7GUV4kFp31p1k_rrs_ttLvlf-pi49ys5iK6epfGu-ceCiewQCfpsjYO-oNUPNnYfxygr660nDNt_qPUCSS7w9ApMr_dWrOFxlz3P2Gw50vHupAmoaGBxAIzuPB-_zLltQaHdOQMVPpnmcktu8O1G2zTP6a7k53TTgNsNdYMM-TUsdzGFSeoK2ccHJazFNor9h4WxS7H9g13hOVazueOcA';

@Injectable({
  providedIn: 'root'
})
export class HttpService {  
  baseUrl = 'https://api.contracts.biv-ot.dev';

  constructor(private http: HttpClient) {
  }

  getEmployees(): Observable<Employee> {
    return this.http.get<Employee>(
      this.baseUrl + '/v1/employee',
      { headers: new HttpHeaders(
        {
          'Authorization': 'Bearer ' + auth_token,
          'Content-Type': 'application/json'
        })
      }
    );
  }

  getEmployee(id: any): Observable<Employee> {
    return this.http.get<any>(
      this.baseUrl + '/v1/employee/' + id,
      { headers: new HttpHeaders(
        {
          'Authorization': 'Bearer ' + auth_token,
          'Content-Type': 'application/json'
        })
      }
    );
  }

  addEmployee(element: any): Observable<Employee> {
    return this.http.post<Employee>(
      this.baseUrl + '/v1/employee',
      JSON.stringify(element),
      { headers: new HttpHeaders(
        {
          'Authorization': 'Bearer ' + auth_token,
          'Content-Type': 'application/json'
        })
      }
    )
  }

  editEmployee(element: any, id: string): Observable<Employee> {
    return this.http.put<Employee>(
      this.baseUrl + '/v1/employee/' + id,
      JSON.stringify(element),
      { headers: new HttpHeaders(
        {
          'Authorization': 'Bearer ' + auth_token,
          'Content-Type': 'application/json'
        })
      }
    );
  }

  deleteEmployee(id: string): Observable<Employee> {
    return this.http.delete<Employee>(
      this.baseUrl + '/v1/employee/' + id,
      { headers: new HttpHeaders(
        {
          'Authorization': 'Bearer ' + auth_token,
          'Content-Type': 'application/json'
        })
      }
    );
  }
}


